//
//  FTPProgress.m
//  VideoUploader
//
//  Created by David Sawyer on 5/16/12.
//  Copyright (c) 2012 Teachscape. All rights reserved.
//

#import "FTPProgress.h"

@implementation FTPProgress

- (id)init
{
    self = [super init];
    
    self.abortTransfer = NO;
    
    return self;
}

- (void)AbortCheck: (BOOL *)abort
{
    *abort = self.abortTransfer;
}

- (void)PercentDone: (NSNumber *)pctDone
abort: (BOOL *)abort
{
    *abort = self.abortTransfer;
    
    if(!self.abortTransfer)
    {
        [self performSelectorOnMainThread:@selector(m_PercentDone:) withObject:pctDone waitUntilDone:YES];
    }
}

- (void)EndUploadFile: (NSStream *)path
numBytes:(NSNumber *)numBytes
{
    [self performSelectorOnMainThread:@selector(m_EndUploadFile:) withObject:numBytes waitUntilDone:YES];
}

- (void)UploadRate: (NSNumber *)byteCount 
       bytesPerSec: (NSNumber *)bytesPerSec
{
    [self performSelectorOnMainThread:@selector(m_UploadRate:) withObject:bytesPerSec waitUntilDone:YES];
    [self performSelectorOnMainThread:@selector(m_BytesUploaded:) withObject:byteCount waitUntilDone:YES];
}

-(void) m_PercentDone:(NSNumber*)pctDone
{
    [_delegate PercentDone:pctDone];
}

-(void) m_EndUploadFile:(NSNumber*)bytes
{
    [_delegate EndUploadFile:bytes];
}

-(void) m_UploadRate:(NSNumber*)bytesPerSec
{
    [_delegate UploadRate:bytesPerSec];
}

- (void)m_BytesUploaded:(NSNumber *)byteCount
{
    [_delegate BytesUploaded:byteCount];
}

@end
