//
//  SFTPProgress.h
//  VideoUploader
//
//  Created by David Sawyer on 5/16/12.
//  Copyright (c) 2012 Teachscape. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CkoSFtpProgress.h"

@protocol SFTPProgressDelegate <NSObject>

@required

- (void)PercentDone: (NSNumber *)pctDone;

- (void)EndUploadFile: (NSNumber *)bytes;

- (void)UploadRate:(NSNumber *)bytesPerSec;

- (void)BytesUploaded:(NSNumber *)byteCount;

@end


@interface SFTPProgress : CkoSFtpProgress

@property(nonatomic, weak) id<SFTPProgressDelegate> delegate;
@property(nonatomic) BOOL abortTransfer;

- (void)AbortCheck: (BOOL *)abort;
- (void)PercentDone: (NSNumber *)pctDone
              abort: (BOOL *)abort;

- (void)UploadRate: (NSNumber *)byteCount
       bytesPerSec: (NSNumber *)bytesPerSec;

- (void)DownloadRate: (NSNumber *)byteCount
         bytesPerSec: (NSNumber *)bytesPerSec;

@end
