//
//  FTPProgress.h
//  VideoUploader
//
//  Created by David Sawyer on 5/16/12.
//  Copyright (c) 2012 Teachscape. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CkoFtp2Progress.h"

@protocol FTPProgressDelegate <NSObject>

@required

- (void)PercentDone: (NSNumber *)pctDone;

- (void)EndUploadFile: (NSNumber *)bytes;

- (void)UploadRate:(NSNumber *)bytesPerSec;

- (void)BytesUploaded:(NSNumber *)byteCount;

@end

@interface FTPProgress : CkoFtp2Progress

@property(nonatomic, weak) id<FTPProgressDelegate> delegate;
@property(nonatomic) BOOL abortTransfer;

- (void)AbortCheck: (BOOL *)abort;

- (void)PercentDone: (NSNumber *)pctDone
              abort: (BOOL *)abort;

//- (void)BeginUploadFile: (NSString *)path 
//                   skip:(BOOL *)skip;

- (void)EndUploadFile: (NSString *)path
             numBytes:(NSNumber *)numBytes;

- (void)UploadRate: (NSNumber *)byteCount 
       bytesPerSec: (NSNumber *)bytesPerSec;

//- (void)ProgressInfo: (NSString *)name
//               value:(NSString *)value;

-(void) m_PercentDone:(NSNumber*)pctDone;
-(void) m_EndUploadFile:(NSNumber*)bytes;
-(void) m_UploadRate:(NSNumber*)bytesPerSec;
-(void) m_BytesUploaded:(NSNumber*)byteCount;

@end
